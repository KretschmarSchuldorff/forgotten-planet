﻿using System;
using SadConsole;
using Microsoft.Xna.Framework;
using Console = SadConsole.Console;
using System.Net;

namespace forgottenplanet
{
    class ForgottenPlanet
    {
        private static FontMaster gameViewFont;
        private static int width = 170;
        private static int mapHeight = 40;
        static void Main()
        {
            Settings.WindowMinimumSize = new Point(1360, 768);
            Settings.ResizeMode = Settings.WindowResizeOptions.Fit;

            // Setup the engine and create the main window.
            SadConsole.Game.Create(170, 48);
            // Hook the start event so we can add consoles to the system.
            SadConsole.Game.OnInitialize = Init;

            // Start the game.
            SadConsole.Game.Instance.Run();
            SadConsole.Game.Instance.Dispose();
        }
        static void Init()
        {
            //GoFullscreen();
            ((SadConsole.Game)SadConsole.Game.Instance).WindowResized += Program_WindowResized;
            gameViewFont = Global.LoadFont("font/mdcurses16.font");

            var noise = PlanetGenerator.BuildMap(width, mapHeight, 1000);
            
            var container = new ContainerConsole();
            var mapView = new Console(width, mapHeight, gameViewFont.GetFont(Font.FontSizes.One), GenerateConsoleSurface(width, mapHeight, noise));
            System.Diagnostics.Debug.WriteLine(mapView.Width * mapView.Height);
            container.Children.Add(mapView);

            var controls = new ControlsConsole(width, 7) { Position = new Point(0, 41) };
            container.Children.Add(controls);
            Global.CurrentScreen = container;
        }
        private static void GoFullscreen()
        {
            Global.GraphicsDeviceManager.HardwareModeSwitch = false;
            Global.GraphicsDeviceManager.IsFullScreen = true;
            Global.GraphicsDeviceManager.ApplyChanges();
        }
        static Cell[] GenerateConsoleSurface(int width, int height, float[] noise)
        {
            var surface = new Cell[width * height];
            for (int i = 0; i < surface.Length; i++)
            {
                if (noise[i] < -0.6f)
                {
                    // Deep water
                    surface[i] = new Cell(Color.DarkBlue, Color.LightBlue, 178);
                }
                else if (noise[i] >= -0.6f && noise[i] < -0.2f)
                {
                    // Shallow Water
                    surface[i] = new Cell(Color.DarkBlue, Color.LightBlue, 177);
                }
                else if (noise[i] >= -0.2f && noise[i] < 0f)
                {
                    // Coastal water
                    surface[i] = new Cell(Color.DarkBlue, Color.LightBlue, 176);
                }
                else if (noise[i] >= 0f)
                {
                    surface[i] = new Cell(new Color(0f + (float)noise[i], 0f + (float)noise[i], 0f + (float)noise[i]), Color.Gray, 30);
                }
            }
            return surface;
        }
        #region Event Managers
        private static void Program_WindowResized(object sender, EventArgs e)
        {
            Global.CurrentScreen.Resize(Global.WindowWidth / Global.CurrentScreen.Font.Size.X, Global.WindowHeight / Global.CurrentScreen.Font.Size.Y, false);
        }
        #endregion
    }
}