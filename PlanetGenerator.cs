﻿using SharpNoise;

namespace forgottenplanet
{
    public static class PlanetGenerator
    {
        public static double[] GenerateSimplex(int width, int height)
        {
            double[] noise = new double[width * height];
            SharpNoise.Modules.Simplex simplexGen = new SharpNoise.Modules.Simplex();
            for (int i = 0; i < width * height; i++)
            {
                noise[i] = simplexGen.GetValue(i, i, 1);
                //System.Diagnostics.Debug.WriteLine($"{x},{y}: {noise[x + y]}");
            }
            return noise;
        }
        public static void GenerateCellNoise(int width, int height, int seed, NoiseMap targetmap)
        {
            SharpNoise.Modules.Cell cellGen = new SharpNoise.Modules.Cell() { Seed = seed, Frequency = 0.25 };
            cellGen.EnableDistance = true;
            for (int y = 0; y < width; y++)
            {
                for (int x = 0; x < height; x++)
                {
                    targetmap[x, y] = (float)cellGen.GetValue(1 + x, 1 + y, 1);
                }
            }
        }
        public static float[] BuildMap(int width, int height, int seed = 0)
        {
            var noiseMap = new NoiseMap(width, width);
            noiseMap.BorderValue = -1;
            GenerateCellNoise(width, height, seed, noiseMap);
            float[] noise = new float[width * height];
            int idx = 0;
            for (int x = 0; x < height; x++)
            {
                for (int y = 0; y < width; y++)
                {
                    noise[idx++] = noiseMap[x,y];
                }
            }
            return noise;
        }
    }
}
